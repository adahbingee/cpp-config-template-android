#include <opencv2/opencv.hpp>
#include "config/manager/ConfigManager.h"
#include "config/config.h"

using namespace cv;
using namespace std;

int main(int argc, char *argv[]) {
    ConfigManager::read();
    
    Mat3b img(300, 300);
    for (int i = 0; i < img.total(); ++i) {
        img(i)[0] = i & 0xFF;
        img(i)[1] = 128;
        img(i)[2] = i & 0xFF;
    }
    imwrite("img.png", img);
    
    ConfigManager::write();
    return 0;
}