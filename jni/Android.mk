LOCAL_PATH := $(call my-dir)
###############################################################################
include $(CLEAR_VARS)
LOCAL_MODULE    := opencv_world
LOCAL_SRC_FILES := ../staticlibs/armeabi-v7a/libopencv_world.a
include $(PREBUILT_STATIC_LIBRARY)
###############################################################################
include $(CLEAR_VARS)
LOCAL_MODULE    := cpufeatures
LOCAL_SRC_FILES := ../staticlibs/armeabi-v7a/libcpufeatures.a
include $(PREBUILT_STATIC_LIBRARY)
###############################################################################
include $(CLEAR_VARS)
LOCAL_MODULE    := libjpeg-turbo
LOCAL_SRC_FILES := ../staticlibs/armeabi-v7a/liblibjpeg-turbo.a
include $(PREBUILT_STATIC_LIBRARY)
###############################################################################
include $(CLEAR_VARS)
LOCAL_MODULE    := libpng
LOCAL_SRC_FILES := ../staticlibs/armeabi-v7a/liblibpng.a
include $(PREBUILT_STATIC_LIBRARY)
###############################################################################

include $(CLEAR_VARS)
LOCAL_MODULE    := out

LOCAL_CFLAGS    += -fPIC -pie -fPIE -O3 -Ofast
LOCAL_CFLAGS    += -I$(LOCAL_PATH)/../include/

LOCAL_LDFLAGS   += -fPIC -pie -fPIE -s
LOCAL_LDLIBS    += -lz -llog -ldl -lm
LOCAL_STATIC_LIBRARIES += opencv_world
LOCAL_STATIC_LIBRARIES += cpufeatures
LOCAL_STATIC_LIBRARIES += libjpeg-turbo
LOCAL_STATIC_LIBRARIES += libpng

LOCAL_SRC_FILES += main.cpp
LOCAL_SRC_FILES += config/config.cpp
LOCAL_SRC_FILES += config/manager/ConfigManager.cpp

include $(BUILD_EXECUTABLE)