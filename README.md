# Requirement
* Clang `5.0.0` above environment. [Clang Pre-Built Binaries](http://releases.llvm.org/download.html)
* Android [NDK](https://developer.android.com/ndk/) r1.7 above

# How to use

1. edit [`jni/config/config.cpp`](jni/config/config.cpp), the supported config types are described in [ConfigManager](https://github.com/adahbingee/config-manager)
1. run [`gen-config.bat`](gen-config.bat)
1. edit `jni/Android.md` and `jni/Application.md` as needed
1. compile code use `ndk-build.bat` or `ndk-build`

# Troubleshooting

## 1. Make sure the binary has executable permissions.

`chmod 777 [file]`

```bash
ls -al
-rwxrwxrwx  1 root root  973020 2018-07-27 16:51 out
```

## 2. If `chmod` doesn't work.

Try use root user, and put the executable file in `/system/` directory. 

```bash
adb root 
adb disable-verity
adb reboot
adb root
adb remount
```

# Reference

## ConfigMaker

[ConfigMaker](https://gist.github.com/adahbingee/33e539264dc4e23dbddb5776bf25a1c1)

A Clang based front-end C++/C preprocessor for auto generating configuration headers.

## ConfigManager

[ConfigManager](https://github.com/adahbingee/config-manager)

A text based configuration manager for C++/C project
